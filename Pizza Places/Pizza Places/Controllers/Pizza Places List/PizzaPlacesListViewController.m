//
//  PizzaPlacesListViewController.m
//  Pizza Places
//
//  Created by Elena on 19.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import "PizzaPlacesListViewController.h"
#import "PizzaPlaceTableViewCell.h"
#import "PizzaPlaceItem.h"
#import "PizzaPlaceDetailsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "CoreDataStack.h"
#import "ItemAndDistance.h"
#import <MBProgressHUD/MBProgressHUD.h>

#define MainUrlString @"https://api.foursquare.com/v2/venues/search?"
#define ClientId @"QG4PYA44EFO1BURJB2TMQEMMBCYW5QP3IP5SC3UZYAAIFJZN"
#define ClientSecret @"0BL02KFJH55WFERUCAUYZDJU14QRARCAFIIOEUE0NEU5PU0B"
#define Version @"20130815"
#define QueryType @"pizza"


@interface PizzaPlacesListViewController () <CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) NSArray <PizzaPlaceItem *>* pizzaPlaces;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) PizzaPlaceItem *pizzaPlaceItem;
@property (nonatomic, strong) CoreDataStack *coreDataStack;
@property (nonatomic, strong) CLLocation *currentUserLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@end

@implementation PizzaPlacesListViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *nib = [UINib nibWithNibName:@"PizzaPlaceTableViewCell" bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:@"PizzaPlaceTableViewCell"];
    self.tableView.estimatedRowHeight = 70.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.titleLabel.text = @"Dear User! Here is the list of the nearest Pizza Restaurants to you!";
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.delegate = self;
    
    if (!([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse)){
        [self.locationManager requestWhenInUseAuthorization];
    }

    NSFetchRequest *fetchRequest  = [NSFetchRequest fetchRequestWithEntityName:@"PizzaPlaceItem"];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
    fetchRequest.sortDescriptors = @[sortDescriptor]; 
    self.pizzaPlaces = [self.coreDataStack.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    if (self.pizzaPlaces.count == 0) {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.label.text  = @"Attempt to receive location. Please make sure that location tracking is enabled in device settings";
        hud.label.numberOfLines = 0;
    }
  }

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    self.currentUserLocation = self.locationManager.location;
    [self getLocationsFromFourSquare:self.currentUserLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self.locationManager requestLocation];
    }
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    [self showErrorAlert];
}

-(CoreDataStack *)coreDataStack{
    if (!_coreDataStack){
        _coreDataStack = [CoreDataStack new];
    }
    return _coreDataStack;
}

# pragma mark - Table View Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return self.pizzaPlaces.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PizzaPlaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PizzaPlaceTableViewCell"];
    cell.pizzaPlaceName.text = self.pizzaPlaces[indexPath.row].name;
    cell.pizzaPlaceDistance.text = [NSString stringWithFormat:@"%0.2f km",[self.pizzaPlaces[indexPath.row].distance floatValue]];
    cell.orderNumber.text =  [NSString stringWithFormat:@"%d",indexPath.row + 1];
   
    return cell;
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.pizzaPlaceItem = [self.pizzaPlaces objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"goToDetails" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"goToDetails"]){
        PizzaPlaceDetailsViewController *detailesController = (PizzaPlaceDetailsViewController *)segue.destinationViewController;
        detailesController.pizzaPlaceItem = self.pizzaPlaceItem;
    }
}

- (void)getLocationsFromFourSquare:(CLLocation*)location{
    NSString *latitude = [NSString stringWithFormat:@"%0.2f", location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%0.2f", location.coordinate.longitude];
    NSString *urlString = [NSString stringWithFormat:@"%@client_id=%@&client_secret=%@&ll=%@,%@,&query=%@&v=%@",MainUrlString,ClientId,ClientSecret,latitude,longitude,QueryType,Version];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSURLSessionDataTask *dataTask = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:urlString] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        

    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (httpResponse.statusCode == 200) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                NSDictionary *response = dictionary[@"response"];
                NSArray *venues = response[@"venues"];
                self.pizzaPlaces = [PizzaPlaceItem arrayOfItemsFromDictionary:venues inManagedObjectContext:self.coreDataStack.managedObjectContext currentLocation:location];
                [self.tableView reloadData];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
                       
            } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showErrorAlert];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        }
    }];
    
    [dataTask resume];
   
}

- (void)showErrorAlert {
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Oops! Something went wrong" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
