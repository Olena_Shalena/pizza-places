//
//  PizzaPlaceDetailsViewController.m
//  Pizza Places
//
//  Created by Elena on 19.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import "PizzaPlaceDetailsViewController.h"
#import "ItemAndDistance.h"


@interface PizzaPlaceDetailsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *pizzaPlaceName;
@property (weak, nonatomic) IBOutlet UILabel *pizzaPlaceDetails;

@end

@implementation PizzaPlaceDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pizzaPlaceName.text = self.pizzaPlaceItem.name;
    self.pizzaPlaceDetails.text = [NSString stringWithFormat:@"%0.2f km",[self.pizzaPlaceItem.distance floatValue]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

# pragma mark  - Actions
- (IBAction)okPressed:(id)sender {
    [self hideController];
}

- (void)hideController{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
