//
//  PizzaPlaceDetailsViewController.h
//  Pizza Places
//
//  Created by Elena on 19.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PizzaPlaceItem.h"
 

@interface PizzaPlaceDetailsViewController : UIViewController

@property (strong, nonatomic) PizzaPlaceItem *pizzaPlaceItem;

@end
