//
//  PizzaPlaceTableViewCell.h
//  Pizza Places
//
//  Created by Elena on 19.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PizzaPlaceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *pizzaPlaceName;
@property (weak, nonatomic) IBOutlet UILabel *pizzaPlaceDistance;
@property (weak, nonatomic) IBOutlet UILabel *orderNumber;

@end
