//
//  PizzaPlaceTableViewCell.m
//  Pizza Places
//
//  Created by Elena on 19.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import "PizzaPlaceTableViewCell.h"

#import "PizzaPlaceItem.h"

@implementation PizzaPlaceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
