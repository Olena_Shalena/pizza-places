//
//  CoreDataStack.h
//  Pizza Places
//
//  Created by Elena on 20.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
@interface CoreDataStack : NSObject
@property (strong) NSManagedObjectContext *managedObjectContext;

- (void)initializeCoreData;
@end
