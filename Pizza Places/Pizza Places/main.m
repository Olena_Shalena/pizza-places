//
//  main.m
//  Pizza Places
//
//  Created by Elena on 19.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
