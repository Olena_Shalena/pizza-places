//
//  PizzaPlaceItem+CoreDataProperties.m
//  Pizza Places
//
//  Created by Elena on 20.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PizzaPlaceItem+CoreDataProperties.h"

@implementation PizzaPlaceItem (CoreDataProperties)

@dynamic name;
@dynamic identifier;
@dynamic lat;
@dynamic lng;
@dynamic distance;

@end
