//
//  ItemAndDistance.h
//  Pizza Places
//
//  Created by Elena on 21.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PizzaPlaceItem.h"
#import "PizzaPlaceItem+CoreDataProperties.h"
@interface ItemAndDistance : NSObject
@property (nonatomic,strong) PizzaPlaceItem *item;
@property (nonatomic, strong) NSNumber *distance;
@end
