//
//  PizzaPlaceItem.m
//  Pizza Places
//
//  Created by Elena on 20.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import "PizzaPlaceItem.h"
#import "CoreDataStack.h"
#import <CoreLocation/CoreLocation.h>



@implementation PizzaPlaceItem

+ (instancetype)itemFromDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context currentLocation:(CLLocation *)location{
    NSEntityDescription *entityDesription = [NSEntityDescription entityForName:@"PizzaPlaceItem" inManagedObjectContext:context];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"identifier = %@",dictionary[@"id"]];
    NSFetchRequest *fetchRequest  = [NSFetchRequest fetchRequestWithEntityName:@"PizzaPlaceItem"];
    fetchRequest.predicate = predicate;
    PizzaPlaceItem *item = [[context executeFetchRequest:fetchRequest error:nil] firstObject];
    if (!item){
        item = [[PizzaPlaceItem alloc] initWithEntity:entityDesription insertIntoManagedObjectContext:context];
        item.identifier = dictionary[@"id"];
    }
    
    item.name = dictionary[@"name"];
    NSDictionary *itemLocation = dictionary[@"location"];
    NSString *latitude  = itemLocation[@"lat"];
    NSString *longitude  = itemLocation[@"lng"];
    item.lat = @([latitude floatValue]);
    item.lng = @([longitude floatValue]);
    item.distance = [item distanceToPizzaPlace:location];
    
    return item;
}


+ (NSMutableArray *)arrayOfItemsFromDictionary:(NSArray *)venues inManagedObjectContext:(NSManagedObjectContext *)context currentLocation:(CLLocation *)location{
    NSMutableArray *arrayOfItemsFromDictionary = [NSMutableArray new];
    for (NSDictionary *dict in venues){
        PizzaPlaceItem *item = [self itemFromDictionary:dict inManagedObjectContext:context currentLocation:location];
        [arrayOfItemsFromDictionary addObject:item];
    }
    [context save:nil];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"distance" ascending:YES];
    [arrayOfItemsFromDictionary sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    return arrayOfItemsFromDictionary;
}


- (NSNumber *)distanceToPizzaPlace:(CLLocation *)location{
    CLLocation *itemLocation = [[CLLocation alloc] initWithLatitude:(CLLocationDegrees)[self.lat doubleValue] longitude:(CLLocationDegrees) [self.lng doubleValue]];
    CLLocationDistance itemDistance = ([location distanceFromLocation:itemLocation])/1000;
    NSNumber *itemDistanseNumber  = [NSNumber numberWithDouble:itemDistance];
    return itemDistanseNumber;
}


@end
