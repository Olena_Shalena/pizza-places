//
//  PizzaPlaceItem.h
//  Pizza Places
//
//  Created by Elena on 20.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PizzaPlaceItem : NSManagedObject


+ (instancetype)itemFromDictionary:(NSDictionary *)dictionary inManagedObjectContext:(NSManagedObjectContext *)context currentLocation:(CLLocation *)location;
+ (NSMutableArray *)arrayOfItemsFromDictionary:(NSArray *)venues inManagedObjectContext:(NSManagedObjectContext *)context currentLocation:(CLLocation *)location;
@end


NS_ASSUME_NONNULL_END

#import "PizzaPlaceItem+CoreDataProperties.h"
