//
//  PizzaPlaceItem+CoreDataProperties.h
//  Pizza Places
//
//  Created by Elena on 20.10.16.
//  Copyright © 2016 Elena. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "PizzaPlaceItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface PizzaPlaceItem (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSString *identifier;
@property (nullable, nonatomic, retain) NSNumber *lat;
@property (nullable, nonatomic, retain) NSNumber *lng;
@property (nullable, nonatomic, retain) NSNumber *distance;
@end

NS_ASSUME_NONNULL_END
